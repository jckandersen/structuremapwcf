﻿using System;
using System.ServiceModel;

namespace StructureMapWcf
{
    public class StructureMapServiceHost : ServiceHost
    {
        public StructureMapServiceHost()
        {
        }

        public StructureMapServiceHost(Type serviceType, params Uri[] baseAddresses) :



        base((Type) StructureMap.ObjectFactory.Model.DefaultTypeFor(serviceType), baseAddresses)
        {
        }

        protected override void OnOpening()
        {
            Description.Behaviors.Add(new StructureMapServiceBehavior());
            base.OnOpening();
        }
    }
}